FROM alpine:3.8
RUN apk add --no-cache php7 php7-dom php7-ctype php7-tokenizer php7-xmlwriter php7-xml composer bash
RUN apk add git libssh2 evince exim
